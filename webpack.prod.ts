import { DefinePlugin } from "webpack";
import * as merge from "webpack-merge";

import common from "./webpack.common";

export default merge(common, {
    mode: "production",
    devtool: "source-map",
    optimization: {
        minimize: true,
    },
    plugins: [
        new DefinePlugin({
            "process.env.NODE_ENV": JSON.stringify("production"),
        }),
    ],
});
