import * as path from "path";

export default {
    entry: {
        main: path.join(__dirname, "src", "main", "index.tsx"),
    },
    output: {
        path: path.join(__dirname, "public"),
        publicPath: "/",
        filename: "bundle.js",
    },
    resolve: {
        extensions: [".js", ".ts", ".tsx", ".scss"],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "awesome-typescript-loader",
                        options: {
                            useCache: true,
                        },
                    },
                ],
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader",
                    },
                    {
                        loader: "sass-loader",
                    },
                ],
            },
        ],
    },
};
