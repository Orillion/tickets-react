import { BanInformation } from "./interfaces/BanInformation";

export default class TicketAPI {
    public static GetMyInfo(): BanInformation {
        return {
            duration: 3600, // 1 Hour
            timestamp: Date.now(),
            // tslint:disable:max-line-length
            message: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
            // tslint:enable:max-line-length
        };
    }
}
