import * as React from "react";
import Card from "../components/Card";

const Home = () => (
    <div className="card-container">
        <Card
            external_to="https://wiki.rizon.net/index.php?title=Channel_Bans"
            header="Channel Ban"
            description="Select this option if you have been banned from a channel."
        />
        <Card
            to="/ban"
            header="Network Ban"
            description="Select this option if you have been banned from the network."
        />
        <Card
            to="/sli"
            header="SLI"
            description="Select this option if you want to connect more than 5 clients from the same IP to the network."
        />
    </div>
);

export default Home;
