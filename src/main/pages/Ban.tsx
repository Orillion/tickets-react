import * as React from "react";
import { BanInformation } from "../interfaces/BanInformation";
import BanInfo from "../components/BanInfo";
import TicketAPI from "../api";

interface BanState {
    ban_information?: BanInformation;
    ip_address: string;
    name: string;
    email: string;
    message: string;
    why: string;
}

export default class Ban extends React.Component<{}, BanState> {
    public constructor(props: {}) {
        super(props);

        this.state = {
            email: "",
            ip_address: "",
            name: "",
            message: "",
            why: "",
        };
    }

    public componentWillMount() {
        const info = TicketAPI.GetMyInfo();

        this.setState({ ban_information: info });
    }

    private handleSubmit = (event: React.FormEvent<any>) => {
        event.preventDefault();
    }

    private handleIpChanged = (event: React.FormEvent<HTMLInputElement>) => {
        // @TODO: Fetch new ip information here when input is a valid ip address.

        this.setState({
            ip_address: event.currentTarget.value,
        });
    }

    private handleNameChanged = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            name: event.currentTarget.value,
        });
    }

    private handleEmailChanged = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            email: event.currentTarget.value,
        });
    }

    private handleMessageChanged = (event: React.FormEvent<HTMLTextAreaElement>) => {
        this.setState({
            message: event.currentTarget.value,
        });
    }

    private handleWhyChanged = (event: React.FormEvent<HTMLTextAreaElement>) => {
        this.setState({
            why: event.currentTarget.value,
        });
    }

    private BanInfo() {
        if (this.state.ban_information) {
            return <BanInfo {...this.state.ban_information} />;
        }

        return <span>No information found</span>;
    }

    private Message() {
        if (this.state.ban_information) {
            return null;
        }

        return (
            <div>
                <label htmlFor="message">Ban Message</label>
                <textarea name="message" onChange={this.handleMessageChanged} />
            </div>
        );
    }

    public render() {
        const message = this.Message();
        const banInfo = this.BanInfo();

        return (
            <div className="form-container">
                <h1>Submit a ticket</h1>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="ip">IP Address</label>
                        <input type="text" name="ip" value={this.state.ip_address} onChange={this.handleIpChanged} />
                    </div>
                    <div>
                        <label>Ban Information</label>
                        {banInfo}
                    </div>
                    <div>
                        <label htmlFor="name">Contact Name</label>
                        <input type="text" name="name" onChange={this.handleNameChanged} />
                    </div>
                    <div>
                        <label htmlFor="email">Contact Email</label>
                        <input type="text" name="email" onChange={this.handleEmailChanged} />
                    </div>
                    {message}
                    <div>
                        <label htmlFor="why">Why</label>
                        <textarea name="why" onChange={this.handleWhyChanged} />
                    </div>
                    <input type="submit" value="Appeal" />
                </form>
            </div>
        );
    }
}
