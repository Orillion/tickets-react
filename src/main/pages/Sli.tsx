import * as React from "react";

interface SliState {
    ip_address: string;
    name: string;
    email: string;
    host_count: number;
    why: string;
    ip_changes: boolean;
}

export default class Sli extends React.Component<{}, SliState> {
    public constructor(props: {}) {
        super(props);

        this.state = {
            email: "",
            ip_address: "",
            name: "",
            host_count: 0,
            ip_changes: false,
            why: "",
        };
    }

    private handleSubmit = (event: React.FormEvent<any>) => {
        event.preventDefault();
    }

    private handleIpChanged = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            ip_address: event.currentTarget.value,
        });
    }

    private handleNameChanged = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            name: event.currentTarget.value,
        });
    }

    private handleEmailChanged = (event: React.FormEvent<HTMLInputElement>) => {
        this.setState({
            email: event.currentTarget.value,
        });
    }

    private handleHostsChanged = (event: React.FormEvent<HTMLInputElement>) => {
        const hostCount = parseInt(event.currentTarget.value, 10);

        if (isNaN(hostCount)) {
            return;
        }

        this.setState({ host_count: hostCount });
    }

    private handleWhyChanged = (event: React.FormEvent<HTMLTextAreaElement>) => {
        this.setState({
            why: event.currentTarget.value,
        });
    }

    public render() {
        return (
            <div className="form-container">
                <h1>Submit a ticket</h1>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label htmlFor="ip">IP Address</label>
                        <input type="text" name="ip" value={this.state.ip_address} onChange={this.handleIpChanged} />
                    </div>
                    <div>
                        <label htmlFor="name">Contact Name</label>
                        <input type="text" name="name" onChange={this.handleNameChanged} />
                    </div>
                    <div>
                        <label htmlFor="email">Contact Email</label>
                        <input type="text" name="email" onChange={this.handleEmailChanged} />
                    </div>
                    <div>
                        <label htmlFor="hosts">Number of Hosts</label>
                        <input type="number" min="1" max="500" name="hosts" onChange={this.handleHostsChanged} />
                    </div>
                    <div>
                        <label htmlFor="why">Why</label>
                        <textarea name="why" onChange={this.handleWhyChanged} />
                    </div>
                    <input type="submit" value="Request" />
                </form>
            </div>
        );
    }
}
