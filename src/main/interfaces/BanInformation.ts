export interface BanInformation {
    timestamp: number;
    duration: number;
    message: string;
}
