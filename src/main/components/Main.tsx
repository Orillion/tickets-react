import * as React from "react";
import { Switch, Route } from "react-router";
import Home from "../pages/Home";
import Ban from "../pages/Ban";
import Sli from "../pages/Sli";
import TicketAPI from "../api";
import { BanInformation } from "../interfaces/BanInformation";

interface MainState {
    info?: BanInformation;
}

class Main extends React.Component<{}, MainState> {
    public componentWillMount() {
        const info = TicketAPI.GetMyInfo();

        this.setState({ info });
    }

    public render() {
        return (
            <main>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/sli" component={Sli} />
                    <Route path="/ban" component={Ban} />
                </Switch>
            </main>
        );
    }
}

export default Main;
