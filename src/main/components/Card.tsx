import * as React from "react";
import { Link } from "react-router-dom";

interface CardProps {
    header: string;
    description?: string;
    to?: string;
    external_to?: string;
}

export default class Card extends React.Component<CardProps> {
    constructor(props: CardProps) {
        super(props);
    }

    public render() {
        if (this.props.to) {
            return (
                <Link to={this.props.to} className="card">
                    <h1>{this.props.header}</h1>
                    <p>{this.props.description}</p>
                </Link>
            );
        } else {
            return (
                <a href={this.props.external_to} className="card">
                    <h1>{this.props.header}</h1>
                    <p>{this.props.description}</p>
                </a>
            );
        }
    }
}
