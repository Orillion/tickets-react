import * as React from "react";
import { BanInformation } from "../interfaces/BanInformation";

export default class BanInfo extends React.Component<BanInformation> {
    public render() {
        return (
            <div>
                <div>
                    <div>
                        Timestamp
                    </div>
                    <div>
                        {this.props.timestamp}
                    </div>
                </div>
                <div>
                    <div>
                        Duration
                    </div>
                    <div>
                        {this.props.duration}
                    </div>
                </div>
                <div>
                    <div>
                        Message
                    </div>
                    <div>
                        {this.props.message}
                    </div>
                </div>
            </div>
        );
    }
}
