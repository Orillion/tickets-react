import * as path from "path";
import * as merge from "webpack-merge";
import * as CircularDependencyPlugin from "circular-dependency-plugin";

import common from "./webpack.common";

export default merge(common, {
    mode: "development",
    devtool: "cheap-source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                enforce: "pre",
                loader: "tslint-loader",
                options: {
                    typeCheck: true,
                    emitErrors: true,
                },
            },
        ],
    },
    devServer: {
        contentBase: path.join(__dirname, "public"),
        historyApiFallback: true,
    },
    plugins: [
        new CircularDependencyPlugin({
            // Exclude detection of files based on a RegExp
            exclude: /a\.js|node_modules/,
            // Add errors to webpack instead of warnings
            failOnError: false,
        }),
    ],
});
