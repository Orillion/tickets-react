# Rizon Tickets Frontend

Getting started with development:

```
$ yarn install
$ yarn start
```

If you're using visual studio code make sure that you have all the recommended extensions installed and that you run `yarn start` and then click "Chrome" in the debug window.
